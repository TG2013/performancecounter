﻿using PC.Entities.Concrete;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PC.DataAccess.Concrete
{
    public class PerfCContext : DbContext
    {
        public PerfCContext()
        {
            Database.SetInitializer<PerfCContext>(null);
        }

        public DbSet<Computer> Computers { get; set; }
        public DbSet<Agent> Agents { get; set; }
    }
}
