﻿using PC.DataAccess.Abstract;
using PC.Entities.Concrete;
using Sage.Core.DataAccess.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PC.DataAccess.Concrete
{
    public class EfComputerDal : EfEntityRepositoryBase<Computer, PerfCContext>, IComputerDal
    {
    }
}
