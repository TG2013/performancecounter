﻿using PC.Entities.Concrete;
using Sage.Core.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PC.DataAccess.Abstract
{
    public interface IComputerDal : IEntityRepository<Computer>
    {
    }
}
