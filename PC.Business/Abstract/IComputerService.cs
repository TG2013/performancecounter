﻿using PC.Entities.Concrete;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PC.Business.Abstract
{
    public interface IComputerService
    {
        Computer GetById(Guid id);

        List<Computer> GetAll();
    }
}
