﻿using PC.Business.Abstract;
using PC.DataAccess.Abstract;
using PC.Entities.Concrete;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PC.Business.Concrete
{
    public class AgentManager : IAgentService
    {
        private readonly IAgentDal _agentDal;

        public AgentManager(IAgentDal agentDal)
        {
            _agentDal = agentDal;
        }

        public Agent GetById(Guid id)
        {
            return _agentDal.Get(a => a.Id == id);
        }
    }
}
