﻿using PC.Business.Abstract;
using PC.DataAccess.Abstract;
using PC.Entities.Concrete;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PC.Business.Concrete
{
    public class ComputerManager : IComputerService
    {
        private readonly IComputerDal _computerDal;

        public ComputerManager(IComputerDal computerDal)
        {
            _computerDal = computerDal;
        }

        public List<Computer> GetAll()
        {
            return _computerDal.GetList();
        }

        public Computer GetById(Guid id)
        {
            return _computerDal.Get(c => c.Id == id);
        }
    }
}
