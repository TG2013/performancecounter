﻿using PC.Business.Abstract;
using PC.Business.Concrete;
using PC.DataAccess.Abstract;
using PC.DataAccess.Concrete;
using SimpleInjector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PC.Business.DependencyResolvers.SimpleInjector
{
    public class BusinessPackage
    {
        public static void Resolve(Container container)
        {
            container.Register<IComputerService, ComputerManager>(Lifestyle.Scoped);
            container.Register<IComputerDal, EfComputerDal>(Lifestyle.Scoped);

            container.Register<IAgentService, AgentManager>(Lifestyle.Scoped);
            container.Register<IAgentDal, EfAgentDal>(Lifestyle.Scoped);
        }
    }
}
