﻿using Sage.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PC.Entities.Concrete
{
    public class Computer : BaseEntityClass, IEntity 
    {
        public string Name { get; set; }
    }
}
