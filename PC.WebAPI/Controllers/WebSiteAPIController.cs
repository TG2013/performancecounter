﻿using PC.Business.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace PC.WebAPI.Controllers
{
    public class WebSiteAPIController :ApiController
    {
        private readonly IComputerService _computerService;

        public WebSiteAPIController(IComputerService computerService)
        {
            _computerService = computerService;
        }

        [HttpGet]
        public List<string> Get() 
        {
            var k = _computerService.GetAll();

            return new List<string> { "value1", "value2" };
            
        }
    }
}