﻿using PC.Business.DependencyResolvers.SimpleInjector;
using SimpleInjector;
using SimpleInjector.Integration.WebApi;
using SimpleInjector.Lifestyles;
using System.Reflection;
using System.Web.Http;

namespace PC.WebAPI.Infrastructure
{
    public class SimpleInjectorHelper
    {
        public static void Resolve(Container container)
        {
            container.Options.DefaultScopedLifestyle = new AsyncScopedLifestyle();
            BusinessPackage.Resolve(container);

            container.RegisterWebApiControllers(GlobalConfiguration.Configuration);
            container.Verify();

            GlobalConfiguration.Configuration.DependencyResolver = new SimpleInjectorWebApiDependencyResolver(container);
        }
    }
}