﻿using SimpleInjector;

namespace PC.WebAPI.Infrastructure
{
    public class IocContainer
    {
        private IocContainer() { }

        public static Container _container;

        public static Container Instance
        {
            get
            {
                if (_container == null)
                    _container = new Container();

                return _container;
            }
        }
    }
}