﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PerformanceTest
{
    class Program
    {
        static void Main(string[] args)
        {
            SysParams s = new SysParams();
            s.getMemAvailable();
            s.getDiskRead();
            s.getDiskAverageTimeRead();

            Console.WriteLine("MEMAvailable" + s.MEMAvailable);
            Console.WriteLine("DISKAverageTimeRead" + s.DISKAverageTimeRead);

            Console.ReadKey();
        }
    }
}
